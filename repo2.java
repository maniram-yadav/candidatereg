package com.repo;


import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.query.Query;
import com.mongodb.Mongo;

import document.Candidate;

/**
 *  This class is responsible for interacting with MongoDB database.
 *
 * There is three method namely saveCandidate(), updateCandidate() and getCandidate() 
 * for fetching,updating and getting the candidate from MongoDB database.
 * 
 */
 

@Repository
public class CandidateRepository {

    /** 
    *   In spring autowired annotation is used to initiate and load the object from spring-servlet.xml configuration file.
    *
    *   mongo and morphia object will be loaded from spring configuration file.
    */
	@Autowired
	Mongo mongo;
	@Autowired
	Morphia morphia;
	
	//database name
	String dbName = new String("model");
	
	
	
	/**  
	 * isFirstTime and datasource variable have been declared as static to initiate the datastore object for database connection  only one time 
	 * using getDatastore() method logic.
	 * 
	 *  Due to static variable these two variable are class property. So once initiated their value will exist between different call
	 * from another class or method and same database connection will be used for all the method call or request.
	 * 
	 * isFirstTime variable will be used to verify that datastore object is going to initiate for the first time if it is 1. 
	 * After that its value will be changed to 0 and it will not allow to initiate the datastore object again in getdatastore() methos.
	 * 
	 */
	 
	 
	 
	static int isFirstTime=1;
	static Datastore datastore;
	
	public void getDataStore() {
		
		System.out.print("\nIs First Time : "+isFirstTime);
		System.out.print("\nData Store : "+datastore);
		
		if(isFirstTime==1)
		{
			
			isFirstTime=0;
			datastore= this.morphia.createDatastore(this.mongo,this.dbName);
		}
		}
		
		
		
		
		
		/**
		 * 
		 * saveCandidate() method will save the candidate to the database.
		 * It will create candidateID as primary key automatically after counting the total number of candidate in the database.
		 * 
		 *
		 */
	
	public String saveCandidate(Candidate c) {
		String msg="";
		getDataStore();   // Initiate datastore object if IsFirstTime variable is equal to 1 otherwise it will use old connection
		
		Query<Candidate> q = datastore.find(Candidate.class);   // get all candidate from MongoDB
		List<Candidate> list=q.asList();                        // Convert query object to list
		c.setCandidateId(list.size()+1);                        // set candidate Id to (no. of total candidate+1)
		
		Key<Candidate> savedCandidate = datastore.save(c);      // Save the candidate
		 
		Object id= savedCandidate.getId();                      // Get saved candidate id
		if(id==null)
			msg="Unable to save Data";
		else 
			msg="Data saved successfully";
		
		return msg;
			
	}


/**
 *  updateCandidate will update the candidate to database.
 * 
 */

	public String updateCandidate(Candidate c) {
		String msg="";
		getDataStore();                  // Initiate datastore object if IsFirstTime variable is equal to 1 otherwise it will use old connection
		
		Key<Candidate> savedCandidate = datastore.save(c);  // Save updated candidate Object to database.
		 
		Object id= savedCandidate.getId();                  // Get saved Object id
		if(id==null)
			msg="Unable to update Data";
		else 
			msg="Data updated successfully";
		
		return msg;
			
	}




/**
 * getCandidate method will return candidate from database with given candidateId.
 * 
 * The number of candidate with given candidateID will be equal to 1.
 * If the list size is zero then there is no candidate with given candidate id and then method will return null object.
 * 
 */

public Candidate getCandidate(Candidate c) {
		
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class,"candidateId",c.getCandidateId());
		List<Candidate> list=q.asList();
		Iterator<Candidate> it=list.iterator();
		if(list.size()==1)
			c=(Candidate)it.next();
		else 
			c=null;
		return c;
	}


	
}
