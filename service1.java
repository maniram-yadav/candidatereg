package com.service;

import com.repo.CandidateRepository;

import document.Candidate;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

	@Autowired
	CandidateRepository candidateRepository;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	public static final Pattern VALID_MOBILE_PATTERN = Pattern.compile("\\d{10}");
	
	public String validateData(String name,String email,String phone,
			String offerdate,String joinDate,String exp,String  prevSal,String offeredSal) {
		

			
		
			boolean status=true;
			String msg="";
			
			if(name==null||name.equals("")) {
				msg += "\nName is null";
			
			}
			if(!validateEmail(email))
			{
				msg += "\nInvalid Email";
			
			}
			if(!validateMobile(phone))
			{
				msg += "\nInvalid Mobile Number";
			
			}
		
		
		
			if(offerdate==null||offerdate==""){
			    msg += "\nOffer Date cannot be null";
				
			}
				if(joinDate==null||joinDate==""){
			    msg += "\nJoining Date cannot be null";
			
			}
			
			try{
			    Date d1=new Date(offerdate);
			    Date d2=new Date(joinDate);
			}
			catch(Exception ex){
			       msg += "\nDate format should be dd/mm/yyyy";
			
			}
			
		
		
		
		
		
			if(exp==null||prevSal==null||offeredSal==null){
			    msg += "\nInvalid integer value ( experience, prevSalary or offeredSalary)";
			}
			else{
			    
            try{
                Integer.parseInt(exp);
                Integer.parseInt(prevSal);
                Integer.parseInt(offeredSal);
            }
            catch(Exception ex){
                msg += "\nNumber format exception";
            }
			    
			}

        if(msg=="")
            msg="correct";
        	
			return msg;
	}
	

	public String save(Candidate c) {
		
		return candidateRepository.saveCandidate(c);
	}
	
	public Candidate get(Candidate c) {
		
		return candidateRepository.getCandidate(c);
	}
	
	public String getAll(){
		String candidates="";
		List<Candidate> list=candidateRepository.getAllCandidate();
		if(list.size()==0)
			candidates="No Data Found";
		else
			for(Candidate candidate:list) {
			candidates += candidate.toString()+"\n";
		}
		return candidates;
	}
	
	public Candidate updateCandidateObject(name,email,phone,offerdate,joindate,exp,prevsal,offersal){
	    Candidate c=new candidate();
	    c.setName(name);
	    c.setName(email);
	    c.setName(phone);
	    c.setName(new Date());
	    c.setName(new Date());
	    c.setName(Integer.parseint(exp));
	    c.setName(Integer.parseint(prevsal));
	    c.setName(Integer.parseint(offersal));
	    
	    return c;
	}
	public Candidate updateCandidateObject(Candidate  c,String finallyJoined,String offerAcceptedDate,String offerDeclinedDate){
	    
	    
	    if(finallyJoined=="false")
	        {
	            c.set
	            c.set
	        }
	    else
	        {
	            c.set
	            c.set
	        }
	        return c;
	}
	
	
	
	
	
		public String validateUpdatableData(String finallyJoined,String offerAcceptedDate,String offerDeclinedDate){
		    
		   
		    String msg="true";
		    if(finallyJoined==null||finallyJoined.equals("")){
		        msg="Null value not accepted.";
		    }
		    else{
		        
		    finallyJoined=finallyJoined.toLowercase();
		    if(finallyJoined.equals("true")){
		        
		        try{
		           Date dd=new Date(offerAcceptedDate);
		        }
		        catch(Exception ex){
		            msg="Invalid date format";
		        }
		        
		    }
		    else if(finallyJoined.equals("false")){
		        try{
		           Date dd=new Date(offerDeclinedDate);
		        }
		        catch(Exception ex){
		            msg="Invalid date format";
		        }
		    }
		    else{
		        msg="Finally Joined either should be true or false";
		    }
		     
		     
		    }
		  return msg;
		  
		}
	
	
	
	public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
}
	public static boolean validateMobile(String mob) {
        Matcher matcher = VALID_MOBILE_PATTERN .matcher(mob);
        return matcher.find();
}
}
