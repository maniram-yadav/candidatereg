package com.controller;

import com.service.CandidateService;

import document.Candidate;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 *  This class will work as controller layer of the application.
 * 
 * It will handle all the upcoming request and will send corresponding message to the front end in JSON format.
 * 
 */
 
 
@Controller
public class CandidateController {

    // initiate candidateService object from spring configuration file
	@Autowired
	CandidateService candidateService;
	
	
	@ResponseBody
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String start() {
		return "Candidate Management RESTful Web API";
	}

	
	/**
	 * This method will handle post request for user registration.
	 * 
	 * It will accept the given parameter and will perform data validation using service layer method.
	 * After validation it will save data and will return corresponding messsage in JSON format
	 * 
	 */ 
	 
	 
	@ResponseBody
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public String register(
			@RequestParam("name") String name,
			@RequestParam("email") String email,
			@RequestParam("phone") String phone,
			@RequestParam("offerdate") String offerdate,
			@RequestParam("joindate") String joindate,
			@RequestParam("experience") int exp,
			@RequestParam("previoussal") long prevsal,
			@RequestParam("offeredsal") long offersal) {

		Candidate c;
		String status=candidateService.validateData(name, email, phone, offerdate, joindate, exp, prevsal, offersal);
		
		if(status.equalsIgnoreCase("correct")) {
			c=candidateService.getCandidateObject(name, email, phone, offerdate, joindate, exp, prevsal, offersal);
			status=candidateService.save(c);
			
		}
		return JSONObject.quote(status);
	}
	


	/**
	 * This method will handle put request for user updation.
	 * 
	 * It will accept the given parameter and will perform data validation.
	 * After validation it will update the data by calling service layer method 
	 * and will return corresponding messsage in JSON format
	 * 
	 */ 
	 
	 
	@ResponseBody
	@RequestMapping(value="/update",method=RequestMethod.PUT)
	public String update(
			@RequestParam("candidateId") int candidateId,
			@RequestParam("isjoined") String finallyJoined,
			@RequestParam("offaccptdate") String offerAcceptedDate,
			@RequestParam("offdecdate") String offerDeclinedDate) {
		
		String status="No any candidate found with this candidate id";
		
		Candidate c=new Candidate();
		String isvalidData="";
		isvalidData=candidateService.validateUpdatableData(finallyJoined, offerAcceptedDate, offerDeclinedDate);
		
		if(isvalidData.equals("true"))
		{
		c.setCandidateId(candidateId);
		c=candidateService.get(c);
		if(c!=null)
		{
			c=candidateService.updateCandidateObject(c, finallyJoined, offerAcceptedDate, offerDeclinedDate);
			status=candidateService.update(c);
		}}
		else {
			status=isvalidData;
		}
		status=JSONObject.quote(status);
	    
		return status;
	}
	

}
