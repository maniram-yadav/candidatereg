package com.repo;


import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.query.Query;
import com.mongodb.Mongo;

import document.Candidate;

/**
 * This class will be used for database interaction purpose. 
 * From object of this class we can fetch and save records to Mongodb database.
 * In spring framework we annotate these class with @Repository annotation.
 * */

@Repository
public class CandidateRepository {

	@Autowired
	Mongo mongo;        // load the mongo bean class from spring-servlet.xml file.
	@Autowired
	Morphia morphia;    // load the morphia bean class from spring-servlet.xml file.
	
	String dbName = new String("bank");   //  name of the database where data will be stored. 
	
	/**
	 *  I have declared isFirstTime and datasource as static. because we have to initiate datasource object only one time.
	 * isFirstTime will be used for validation purpose. If it is 1 it means datasource has been initiated.
	 * /
	
	
	static int isFirstTime=0;
	static Datastore datastore;     
	
	public void getDataStore() {
		if(isFirstTime==0)
		{
			isFirstTime=1;
		    datastore= this.morphia.createDatastore(this.mongo,this.dbName);
		}
		}
	
	
	public String saveCandidate(Candidate c) {
		String msg="";
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class);
		List<Candidate> list=q.asList();
		c.setCandidateId(list.size()+1);
		
		Key<Candidate> savedCandidate = datastore.save(c);
		 
		Object id= savedCandidate.getId();
		if(id==null)
			msg="Unable to save Data";
		else 
			msg="Data saved successfully";
		
		return msg;
			
	}
	public Candidate getCandidate(Candidate c) {
		
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class,"name",c.getName());
		List<Candidate> list=q.asList();
		Iterator<Candidate> it=list.iterator();
		if(list.size()==1)
			c=(Candidate)it.next();
		else 
			c=null;
		return c;
	}
public List<Candidate> getAllCandidate() {
		
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class);
		List<Candidate> list=q.asList();
		return list;
	}
	
}
