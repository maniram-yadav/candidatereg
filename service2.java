package com.service;

import com.repo.CandidateRepository;

import document.Candidate;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * This is service layer. It will work as middleware between controller and model layer in this project.
 * 
 * It will also be used for validation purpose of data and will provide some additional feature to controller layer.
 * 
 */ 

@Service
public class CandidateService {

    // initiate CandidateRepository object from spring configuration file
	@Autowired
	CandidateRepository candidateRepository;
	
	//regex pattern for validating email
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX =  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	//regex pattern for validating mobileno
	public static final Pattern VALID_MOBILE_PATTERN = Pattern.compile("^[6-9][0-9]{9}$");
	
	
	/**
	 * 
	 * validateData method will be used to validate the given value passsed to it and after that it will return corresponding model.
	 * 
	 */ 
	
	public String validateData(String name,String email,String phone,
			String offerdate,String joinDate) {
		
			boolean status=true;
			String msg="";
			
			if(name==null||name.equals("")) {
				msg += "\nName is null";
			
			}
			if(!validateEmail(email))
			{
				msg += "\nInvalid Email";
			
			}
			if(!validateMobile(phone))
			{
				msg += "\nInvalid Mobile Number";
			
			}
		
			if(offerdate==null||offerdate==""){
			    msg += "\nOffer Date cannot be null";
				
			}
				if(joinDate==null||joinDate==""){
			    msg += "\nJoining Date cannot be null";
			
			}
			
			try{
			    Date d1=new Date(offerdate);
			    Date d2=new Date(joinDate);
			}
			catch(Exception ex){
			       msg += "\nDate format should be dd/mm/yyyy";
			}
			
		
        if(msg=="")
            msg="correct";
        	
			return msg;
	}
	
// This method will save candidate data in database using CandidateRepository class

	public String save(Candidate c) {
		
		return candidateRepository.saveCandidate(c);
	}
	
	
	
	// This method will update candidate data in database using CandidateRepository class
    public String update(Candidate c) {
		
		return candidateRepository.updateCandidate(c);
	}
	
	
	
	// This method will fetch candidate with given CandidateId from CandidateRepository class and it will return the candidate object.
	public Candidate get(Candidate c) {
		
		return candidateRepository.getCandidate(c);
	}

	
	
	
	/**
	 * getCandidateObject method will return Candidate object after setting the property of candidate object.
	 * 
	 */ 
	 
	 
	public Candidate getCandidateObject(String name,String email,String phone,String offerdate,
			String joindate,int exp,long prevsal,long offersal){
	    Candidate c=new Candidate();
	    c.setName(name);
	    c.setEmailId(email);
	    c.setPhoneNumber(phone);
	    c.setCreationDateTime(new Date());
	    c.setOfferDate(new Date(offerdate));
	    c.setJoiningDate(new Date(joindate));
	    c.setExperience(exp);
	    c.setLastModifiedDateTime(new Date());
	    c.setOfferedSalary(offersal);
	    c.setPreviousSalary(prevsal);
	    return c;
	}
	
	
	/**
	 * updateCandidateObject method will set finallyJoined and updateCandidateObject or offerDeclinedDate property of Candidate class
	 * based on the value of finallyJoined variable and this method will return updated candidate object.
	 * 
	 * 
	 */ 
	
	public Candidate updateCandidateObject(Candidate  c,String finallyJoined,String offerAcceptedDate,String offerDeclinedDate){
	    
	    c.setLastModifiedDateTime(new Date());
	    if(finallyJoined.equals("false"))
	        {
	            c.setFinallyJoined(false);
	            c.setOfferDeclinedDate(new Date(offerDeclinedDate));
	        }
	    else
	        {
            c.setFinallyJoined(true);
            c.setOfferAcceptedDate(new Date(offerAcceptedDate));
	        }
	        return c;
	}
	
	
	
	
	    /**
	     * validateUpdatableData method will validate the data which is to update in candidate table.
	     * 
	     * if finallyJoined is true then it will only offerAcceptedDate for validation otherwise
	     * it will only offerDeclinedDate for validation
	     * 
	     */ 
	     
	     
		public String validateUpdatableData(String finallyJoined,String offerAcceptedDate,String offerDeclinedDate){
		    
		   
		    String msg="true";
		    if(finallyJoined==null||finallyJoined.equals("")){
		        msg="Null value not accepted.";
		    }
		    else{
		        
		    finallyJoined=finallyJoined.toLowerCase();
		    if(finallyJoined.equals("true")){
		        
		        try{
		           Date dd=new Date(offerAcceptedDate);
		        }
		        catch(Exception ex){
		            msg="Invalid accepted date format";
		        }
		        
		    }
		    else if(finallyJoined.equals("false")){
		        try{
		           Date dd=new Date(offerDeclinedDate);
		        }
		        catch(Exception ex){
		            msg="Invalid declined date format";
		        }
		    }
		    else{
		        msg="Finally Joined should be either true or false";
		    }
		    }
		  return msg;
		  
		}
	
	
	
	/**
         * validateEmail method will return true if given email id is in correct format.
         */
	public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
}


        /**
         * validateMobile method will return true if given mobile no. is in correct format.
         */ 
	public static boolean validateMobile(String mob) {
        Matcher matcher = VALID_MOBILE_PATTERN .matcher(mob);
        return matcher.find();
}
}

