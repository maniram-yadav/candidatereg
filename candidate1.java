package com;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;
import com.service.CandidateService;

import document.Candidate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class CandidateController {

	@Autowired
	CandidateService candidateService;
	
	@ResponseBody
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String hello() {
		return "Hello";
	}
	@ResponseBody
	@RequestMapping(value="/home",method=RequestMethod.GET)
	public String home() {
		return "Home";
	}
	
	@ResponseBody
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public String register() {
			@RequestParam("name") String name,
			@RequestParam("email") String email,
			@RequestParam("phone") String phone,
			@RequestParam("offerdate") String offerdate,
			@RequestParam("joindate") String joinDate,
			@RequestParam("experience") String exp,
			@RequestParam("previoussal") String prevsal,
			@RequestParam("offeredsal") String offersal) {
     
     
     String status=candidateService.validateData( name, email, phone,
			 offerdate, joindate, exp,  prevSal, offeredsal) ;

		if(status.equals("correct"){
		c1=candidateService.updateCandidateObject(name,email,phone,offerdate,joinDate,exp,prevsal,offersal);
		
// 		c.setName("Mahesh");
// 		c.setExperience(0);
// 		Candidate c1=candidateService.get(c);
// 		c1.setCandidateId(1);
// 		c1.setName("Anamika");
		
		String msg=candidateService.save(c1);    
		}
		else{
		msg=status;
		}
		return msg;
	}
	

	@ResponseBody
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(
			@RequestParam("candid") String candidateId,
			@RequestParam("finjoined") String finallyJoined,
			@RequestParam("offerdate") String offerAcceptedDate,
			@RequestParam("offerdecdate") String offerDeclinedDate) {
		
		Candidate c1;
		String msg;
		
		c1=candidateService.updateCandidateObject(finallyJoined,offerAcceptedDate,offerDeclinedDate);
		if(c1!=null){
		c1=candidateService.get(c1);
		msg=candidateService.save(c);
		}
		else{
		msg="Invalid data";    
		}
		
		
		return msg;
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public String getAllCandidate() {
		
		
		String allCandidates=candidateService.getAll();
		return allCandidates;
	}
	


@ExceptionHandler(IllegalArgumentException.class)
	public String handleIllegalArgumentException(HttpServletRequest request, Exception ex){
	
    return "Less no of Parameter is passed in the request. Exception : "+ex.toString() +" at "+request.getRequestURL();
}

@ExceptionHandler(Exception.class)
	public String handleException(HttpServletRequest request, Exception ex){
	
    return "Following Exception occcured. Exception : "+ex.toString() +" at "+request.getRequestURL();
}
	
	
	
	
	/*
	 * .       Name - Mandatory

2.       Emailm- Mandatory

3.       PhoneNumber - Mandatory

4.       OfferDate - Mandatory

5.       JoiningDate - Mandatory

6.       Experience - Optional

7.       PreviousSalary – Optional

8.       OfferedSalary- Optional

9.       CreationDateTime – Hide this from user i.e. do not send in response

10   LastModifiedDateTime – Hide this from user i.e. do not send in response

11   FinallyJoined - Boolean (To be used in DTO, read full task first)

12   OfferAcceptedDate – If Finally Joined is false (To be used in DTO, read full task first)

13   OfferDeclinedDate – If Finally Joined is false (To be used in DTO, read full task first)

14   CandidateId (PrimaryKey)

ThThe task is to develop a Spring MVC based RESTful API to create Candidate record.
 Make a DTO with following fields to update the candidate and create RESTful API to find and update the Candidate record.

1.       CandidateId

2.       FinallyJoined

3.       OfferAcceptedDate

4.       OfferDeclinedDate

 
	 * */
}
