package com.repo;


import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.query.Query;
import com.mongodb.Mongo;

import document.Candidate;

@Repository
public class CandidateRepository {

	@Autowired
	JdbcTemplate jdcTemplate;
	@Autowired
	Mongo mongo;
	@Autowired
	Morphia morphia;
	
	String dbName = new String("bank");
	static int isFirstTime=0;
	Datastore datastore;
	
	public void getDataStore() {
		if(isFirstTime==0)
		{
			isFirstTime=1;
			this.datastore= this.morphia.createDatastore(this.mongo,this.dbName);
		}
		}
	
	
	public String saveCandidate(Candidate c) {
		String msg="";
		getDataStore();
		
		 Key<Candidate> savedCandidate = datastore.save(c);
		 
		Object id= savedCandidate.getId();
		if(id==null)
			msg="Unable to save Data";
		else 
			msg="Data saved successfully";
		
		return msg;
			
	}
	public Candidate getCandidate(Candidate c) {
		
		
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class,"name",c.getName());
		List<Candidate> list=q.asList();
		Iterator<Candidate> it=list.iterator();
		if(list.size()==1)
			c=(Candidate)it.next();
		else 
			c=null;
		return c;
	}
public List<Candidate> getAllCandidate() {
		
		getDataStore();
		
		Query<Candidate> q = datastore.find(Candidate.class);
		List<Candidate> list=q.asList();
		return list;
	}
	
}
