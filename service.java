package com.service;

import com.repo.CandidateRepository;

import document.Candidate;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

	@Autowired
	CandidateRepository candidateRepository;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	public static final Pattern VALID_MOBILE_PATTERN = Pattern.compile("\\d{10}");
	
	public String validateData(String name,String email,String phone,
			String offerdate,String joinDate,int exp,int  prevSal,int offeredSal) {
		

			
		
			boolean status=true;
			String msg="";
			if(name==null||name.equals("")) {
				msg += "\nName is null";
				status=false;
			}
			if(!validateEmail(email))
			{
				msg += "\nInvalid Email";
				status=false;
			}
			if(!validateMobile(phone))
			{
				msg += "\nInvalid Mobile Number";
				status=false;
			}

			
			return "yes";
	}
	

	public String save(Candidate c) {
		
		return candidateRepository.saveCandidate(c);
	}
	
	public Candidate get(Candidate c) {
		
		return candidateRepository.getCandidate(c);
	}
	
	public String getAll(){
		String candidates="";
		List<Candidate> list=candidateRepository.getAllCandidate();
		if(list.size()==0)
			candidates="No Data Found";
		else
			for(Candidate candidate:list) {
			candidates += candidate.toString()+"\n";
		}
		return candidates;
	}
	
	
	
	public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
}
	public static boolean validateMobile(String mob) {
        Matcher matcher = VALID_MOBILE_PATTERN .matcher(mob);
        return matcher.find();
}
}
